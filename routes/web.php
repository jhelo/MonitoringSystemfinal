<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/QR-Login', 'QRController@index')->name('qrlogin');
Route::post('/QR-Login', 'QRController@checkUser');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/update', 'HomeController@updateProfile');
Route::get('/profile', 'HomeController@qrcode')->name('viewQR');
Route::group(['prefix' => 'user'], function(){
	Route::get('/sites/{id}', 'HomeController@visit_count');
});
Route::get('/live_search', 'LiveSearch@index');
Route::get('/live_search/action', 'LiveSearch@action')->name('live_search.action');

Route::group(['prefix' => 'admin'],function (){
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::get('/', 'AdminController@index')->name('admin.dashboard');
	Route::get('/', 'AdminController@education')->name('admin.dashboard');


	
	Route::get('/terminal', 'TerminalController@index')->name('admin.terminal');
	Route::get('/users', 'UsersController@index')->name('admin.users');
	Route::get('/users/remove/{id}', 'UsersController@destroy');
	Route::post('/users/info', 'UsersController@view');
	Route::post('/users/search', 'UsersController@search');
	
	
	Route::get('/logs', 'LogsController@index')->name('admin.logs');
	Route::post('/logs/users', 'LogsController@getUsersLogs');
	Route::get('/visits', 'AdminController@getVisits');
	Route::get('/sites/{id}', 'SitesController@show');
	Route::post('/sites/info', 'SitesController@view');
	Route::get('/sites', 'SitesController@index')->name('admin.sites');

	Route::get('/{section}/logs', 'LogsController@timeIns')->name('admin.stats.timein');

});




Route::group(['prefix' => 'terminal'], function(){
	Route::post('/store', 'TerminalController@store');
});


Route::group(['prefix' => 'sites'], function(){
	Route::get('/{id}', 'SitesController@showSite');
	Route::post('/store', 'SitesController@store');
	Route::get('/remove/{id}', 'SitesController@destroy');
});

Route::group(['prefix' => 'users'], function(){
	Route::get('/{id}', 'UsersController@showSite');
	Route::post('/store', 'UsersController@store');
	Route::get('/remove/{id}', 'UsersController@destroy');
});

Route::group(['prefix' => 'timein'], function(){
	Route::get('/', 'TimeinController@index')->name('timein');
	Route::post('/', 'TimeinController@checkUser')->name('timein.submit');
	Route::post('/qr-timein', 'QRController@checkTimein');
});

Route::get('/registerqr', function () {
    return view('registerqr');

});


Route::group(['prefix' => 'registration'], function(){
	Route::get('/', 'registrationController@index')->name('registration');
});
