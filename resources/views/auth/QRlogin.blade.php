@extends('layouts.app')

@section('jshead')
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/3.3.3/adapter.min.js"></script> -->
@endsection

@section('content')<br><br><br><br>
<div class="container">
    <center><img src="{{asset('img/ms.png')}}" height="150" alt=""><br>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    <video id="preview"></video>

					<div class="modal" tabindex="-1" role="dialog" id="notif">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-body">
								<p>Modal body text goes here.</p>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript" src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<script type="text/javascript">
	let scanner = new Instascan.Scanner({
		video: document.getElementById('preview')
	});

	scanner.addListener('scan', function(content){
		let form = new FormData();
		form.append('code', content);

		axios.post('/QR-Login', form)
		.then(function(response){
			console.log(response);
			if(response.data == 1){
				swal({
					type: 'success',
					title: 'Successfully Login',
					showConfirmButton: false,
					timer: 3000
				})
				setTimeout(function(){
					$(location).attr('href', '{{url('/')}}');
				}, 3000);
			}
		})
		.catch(function(error){
			console.log(error);
		});
	});

	Instascan.Camera.getCameras().then(function(cameras){
		if(cameras.length > 0){
			scanner.start(cameras[0]);
		}else{
			console.log('error');
		}
	});
</script>
@endsection