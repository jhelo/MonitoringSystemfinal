@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card-columns">
        @if(count($sites)>0)
            @foreach($sites as $site)
            <div class="card shadow">
                <div class="card-body">
               
                    <h5 class="card-title" style="text-align: center;">{{$site->site_name}}</h5>
                    <a href="#" class="btn btn-info shadow" style="width: 300px;" onclick="viewSite({{$site->id}}); ">Visit</a>
                </div>
               
            </div>
            @endforeach
        @endif
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">

    function viewSite(id){
        axios.get('/user/sites/'+id)
        .then(function(response){
            var MyPopUp = false;
            OpenWindow(response.data.url, response.data.site_name);
            function OpenWindow(url,name){
            //checks to see if window is open
                if(MyPopUp && !MyPopUp.closed){
                    winPop.focus(); //If already Open Set focus
                }
                else{
                    MyPopUp = window.open(url, '_blank');//Open a new PopUp window
                }
            }
        })
        .catch(function(error){
            console.log(error);
        });
    }
</script>
@endsection