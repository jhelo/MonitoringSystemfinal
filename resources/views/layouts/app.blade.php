<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'QCPL-MonitoringSystem') }}</title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">

    
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    @yield('style')
    @yield('jshead')

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark " style="background-color: #2d3e50;">
            <div class="container ">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{asset('img/ms.png')}}" height="60" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li><a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a></li>
                            <li><a class="nav-link" href="{{ route('qrlogin') }}">{{ __('QR Login') }}</a></li>
                          
                           

                        @else
                            <!-- <li><a class="nav-link" href="{{route('viewQR')}}">{{ __('Profile') }}</a></li> -->


                             <div class="btn-group mr-1">
                            <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      {{ Auth::user()->name }}
                                 </button>
                                 <div class="dropdown-menu">
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#profileModal">Profile</a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#qrModal">QR Code</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                                </div>


                            <li class="nav-item dropdown">
                                

                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#profileModal">Profile</a>
                                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#qrModal">QR Code</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>


                                <div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Profile</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
  <center>
                                        <div class="card modal-body" >
                                            @if(Auth::user()->profilePicture)
                                          
                                            <img class="card-img-top rounded-circle shadow" src="/img/user_profile/{{Auth::user()->profilePicture}}" alt="Profile Picture" style="width:230px; height: 230px;">
                                            @else
                                            <img class="card-img-top rounded-circle shadow" src="/img/user_profile/user.png" alt="Profile Picture" style="width:230px; height: 230px;"></center>
                                            @endif
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="picture">Profile Picture</label>
                                                    <input type="file" id="picture" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input type="text" id="name" class="form-control" value="{{Auth::user()->name}}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="text" id="email" class="form-control" value="{{Auth::user()->email}}" readonly>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary" onclick="updateProfile();">Save changes</button>
                                        </div>
                                        </div>
                                        
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="modal fade" id="qrModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Your QR Code</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body text-center">
                                            <img id="myqr" class="shadow" src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->color(38, 38, 38, 0.85)->backgroundColor(255, 255, 255, 0.82)->size(200)->generate(Auth::user()->QRpassword)) !!} ">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <a href="#" class="btn btn-primary" onclick="this.href = $('#myqr').attr('src');" download="QR-Code">Download</a>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
        
    </div>


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="{{asset('js/sweetalert2.all.js')}}"></script>

    @if(Auth::user())
    <script>
        function updateProfile(){
            let fileInput = document.querySelector('#picture');
            let form = new FormData();
            form.append('picture', fileInput.files[0]);
            // form.append('email', $('#email').val());
            
            axios.post('/update', form)
            .then(function(res){
                if(res.data == 'success'){
                    swal({
                        type: 'success',
                        title: 'Your profile has been updated',
                        timer: 1500
                    })
                    setTimeout(function(){ location.reload(); }, 1500);
                }
            })
            .catch(function(err){
                console.log(err);
            });
        }
    </script>
    @endif
    @yield('script')
</body>
</html>
