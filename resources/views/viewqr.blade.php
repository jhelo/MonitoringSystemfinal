@extends('layouts.app')

@section('content')
<div class="container text-center">
	<div class="card" style="width: 18rem;">
		@if($user->picture == null)
			<img class="card-img-top rounded-circle" src="{{asset('img/user_profile/user.png')}}" alt="Card image cap">
		@else
			<img class="card-img-top rounded-circle" src="img/user_profile/{{$user->picture}}" alt="Card image cap">
		@endif
		<div class="card-body">
			<h4 class="card-title">{{$user->name}}</h4>
			<h5 class="card-subtitle mb-2 text-muted">{{Auth::user()}}</h5>
		</div>
	</div>
	@if($user->QRpassword)
		<div class="card" style="width: 20rem;">
			<img id="myqr" src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->color(38, 38, 38, 0.85)->backgroundColor(255, 255, 255, 0.82)->size(200)->generate($user->QRpassword)) !!} ">
			<div class="card-body">
				<!-- <h5 class="card-title">Card title</h5>
				<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> -->
				<a href="#" class="btn btn-primary" onclick="this.href = $('#myqr').attr('src');" download>Download</a>
			</div>
		</div>
	@endif
</div>
@endsection

@section('script')
<script>

</script>
@endsection