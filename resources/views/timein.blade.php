@extends('layouts.appTime')

@section('content')<br><br><br>
<div class="container">
 <center><img src="{{asset('img/ms.png')}}" height="150" alt=""></center><br>
    <div class="card-deck">
        <div class="card">
            <div class="card-header"><center><b>Login</b></center></div><br><br>
            <div class="card-body">
                <div class="form-group row">
                    <label for="username" class="col-sm-4 col-form-label text-md-right">Username :</label>
                    <div class="col-md-6">
                        <input id="username" type="username" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="username" class="col-sm-4 col-form-label text-md-right">Password :</label>
                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control">
                    </div>
                </div>
                <div class="form-group row mb-0">
                    <div class="col-md-8 offset-md-4">
                        <button type="button" class="btn btn-primary" onclick="checkUser();" style="width: 234px;">
                            Login
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header"><center><b>Login with QR Code</b></center></div>
            <div class="card-body">
                <video id="preview" style="width: 100%; height: 100%;"></video>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{asset('js/instascan.min.js')}}"></script>
<script type="text/javascript">
let scanner = new Instascan.Scanner({
    video: document.getElementById('preview')
});
scanner.addListener('scan', function(content) {
    let form = new FormData();
    form.append('code', content);
    axios.post('/timein/qr-timein', form).then(function(response) {
        console.log(response);
        if (response.data == 1) {
            swal({
                type: 'success',
                title: 'User TimeIN.',
                showConfirmButton: !1,
                timer: 3000
            })
            setTimeout(function() {
                $(location).attr('href', '{{url('/timein')}}')
            }, 3000)
        }
    }).catch(function(error) {
        console.log(error)
    })
});
Instascan.Camera.getCameras().then(function(cameras) {
    if (cameras.length > 0) {
        scanner.start(cameras[0])
    } else {
        console.log('error')
    }
})
function checkUser(){
    console.log($('#username').val());
    console.log($('#password').val());
    let form = new FormData();
    form.append('username', $('#username').val());
    form.append('password', $('#password').val());
	
	axios.post('/timein', form)
	.then(function(response){
		console.log(response);
		if(response.data == 1){
			swal({
				type: 'success',
				title: 'User TimeIN.',
				showConfirmButton: false,
				timer: 3000
			})
			setTimeout(function(){
				$(location).attr('href', '{{url('/timein')}}');
			}, 3000);
		}else{
			swal({
				type: 'warning',
				title: 'Invalid Credentials.',
				showConfirmButton: false,
				timer: 3000
			})
			setTimeout(function(){
				$(location).attr('href', '{{url('/timein')}}');
			}, 3000);
		}
	})
	.catch(function(err){
		console.log(err);
	});
}
</script>
@endsection