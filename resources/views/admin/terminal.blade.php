@extends('layouts.admin')

@section('content')
<div class="container">
	<div class="my-3 float-right">		
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Add Terminal</button>

		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Add Terminal</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
                        <div class="alert d-none" role="alert"></div>
                        <div class="form-group">
                            <label for="terminalNumber">Terminal Number</label>
                            <input type="text" class="form-control" id="terminalNumber" aria-describedby="terminalNumber" placeholder="Terminal Number">
                        </div>
                        <div class="form-group">
                            <label for="terminalIP">Terminal's IP Address</label>
                            <input type="text" class="form-control" id="terminalIP" aria-describedby="terminalIP" placeholder="IP Address">
                        </div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary" onclick="addTerminal();">Save changes</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div>
		<table class="table table-bordered text-center">
			<thead>
				<tr>
					<th scope="col">Terminal #</th>
					<th scope="col">IP Address</th>
				</tr>
			</thead>
			<tbody>
				@if(count($terminals)>0)
					@foreach($terminals as $terminal)
						<tr>
							<th scope="row">{{$terminal->terminal_number}}</th>
							<td>{{{$terminal->ip_address}}}</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan="2">No Data Found</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	function addTerminal(){
        if($('#terminalNumber').val() == '' || $('#terminalIP').val() == ''){
            $('#save').attr('disabled','disabled');
            $('.alert').addClass('alert-danger').removeClass('d-none').text('Please fill all of the information.');
            setTimeout(function(){
                $('.alert').addClass('d-none').removeClass('alert-danger');
                $('#save').removeAttr('disabled');
            }, 3000);
        }else{
            let form = new FormData();
            form.append('_token', $('input[name=_token]').val());
            form.append('terminalNumber', $('#terminalNumber').val());
            form.append('terminalIP', $('#terminalIP').val());

            axios.post('/terminal/store', form)
            .then(function(response){
            	console.log(response);
                if(response.data == 'success'){
                    $('.alert').addClass('alert-success').removeClass('d-none').text('Record saved.');
                    setTimeout(function(){
                        location.reload();
                    }, 3000);
                }else{
                    $('.alert').addClass('alert-danger').removeClass('d-none').text('An error occured, the site will refresh in 3 seconds.');
                    setTimeout(function(){
                        $('.alert').addClass('d-none').removeClass('alert-danger');
                        location.reload();
                    }, 3000);
                }
            })
            .catch(function(error){
                console.log(error);
            });
        }
	}
</script>
@endsection