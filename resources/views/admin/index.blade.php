@extends('layouts.admin')

@section('style')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
@endsection

@section('content')<br><br><br>


<div class="container ">
<div class="row">
  <div class="col-3">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action list-group-item-light" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Most Visited Sites</a>
      <a class="list-group-item list-group-item-action list-group-item-light"  id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Percentage of Educational Background</a>
      <a class="list-group-item list-group-item-action list-group-item-light"  id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages"> Percentage of Male and Female Users</a>
     
    </div>
  </div>
  <div class="col-8">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
      <div class="card-body"> 
      <div id="piechart" style="width: 700px; height: 500px;"></div>
      </div>    
      </div>
      <div class="tab-pane fade active " id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
          <div class="card-body"> 
      <div id="educchart" style="width: 700px; height: 500px;"></div>
      </div> 
      </div>
           
      <div class="tab-pane fade active " id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
          <div class="card-body"> 
      <div id="genderchart" style="width: 700px; height: 500px;"></div>
      </div> 
      </div>
  
    </div>
  </div>
</div>
</div>

@endsection

@section('script')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

 

    axios.get('/admin/visits')
    .then(function(response){
        // console.log(response.data);
        var sites = response.data;
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Site Name');
            data.addColumn('number', 'Number of Visits');

            for (var i = 0; i < sites.length; i++) {
                data.addRow([sites[i][0], sites[i][1]]);
            }

            console.log(data);
            var options = {
                title: 'Most Visited Sites'
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));

            chart.draw(data, options);
        }




 var what = <?php echo $education; ?>


   google.charts.load('current', {'packages':['corechart']});

   google.charts.setOnLoadCallback(drawChart5);

   function drawChart5()
   {
    var data = google.visualization.arrayToDataTable(what);
    var options = {
     title : 'Percentage of Educational Background'
    };
    var chart = new google.visualization.PieChart(document.getElementById('educchart'));
    chart.draw(data, options);
   }



    })
    .catch(function(error){
        console.log(error);
    });





 

  
</script>
@endsection