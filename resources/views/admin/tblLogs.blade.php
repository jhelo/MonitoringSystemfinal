@if(count($schedules)>0)
    @foreach($schedules as $schedule)
        <tr>
            <th scope="row">{{date('h:i A', strtotime($schedule->start_period))}} - {{date('h:i A', strtotime($schedule->end_period))}}</th>
            <td>
                @php
                    if($gender == 'all'){
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'Prep')
                        ->count();
                        echo $educ;
                    }else{
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'Prep')
                        ->where('gender', $gender)
                        ->count();
                        echo $educ;
                    }
                @endphp
            </td>
            <td>
                @php
                    if($gender == 'all'){
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'Elementary')
                        ->count();
                        echo $educ;
                    }else{
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'Elementary')
                        ->where('gender', $gender)
                        ->count();
                        echo $educ;
                    }
                @endphp
            </td>
            <td>
                @php
                    if($gender == 'all'){
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'Junior High School')
                        ->count();
                        echo $educ;
                    }else{
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'Junior High School')
                        ->where('gender', $gender)
                        ->count();
                        echo $educ;
                    }
                @endphp
            </td>
            <td>
                @php
                    if($gender == 'all'){
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'Senior High School')
                        ->count();
                        echo $educ;
                    }else{
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'Senior High School')
                        ->where('gender', $gender)
                        ->count();
                        echo $educ;
                    }
                @endphp
            </td>
            <td>
                @php
                    if($gender == 'all'){
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'College')
                        ->count();
                        echo $educ;
                    }else{
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'College')
                        ->where('gender', $gender)
                        ->count();
                        echo $educ;
                    }
                @endphp
            </td>
            <td>
                @php
                    if($gender == 'all'){
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'Professional')
                        ->count();
                        echo $educ;
                    }else{
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'Professional')
                        ->where('gender', $gender)
                        ->count();
                        echo $educ;
                    }
                @endphp
            </td>
            <td>
                @php
                    if($gender == 'all'){
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'Senior Citizen')
                        ->count();
                        echo $educ;
                    }else{
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'Senior Citizen')
                        ->where('gender', $gender)
                        ->count();
                        echo $educ;
                    }
                @endphp
            </td>
            <td>
                @php
                    if($gender == 'all'){
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'OSY')
                        ->count();
                        echo $educ;
                    }else{
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'OSY')
                        ->where('gender', $gender)
                        ->count();
                        echo $educ;
                    }
                @endphp
            </td>
            <td>
                @php
                    if($gender == 'all'){
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'PWD')
                        ->count();
                        echo $educ;
                    }else{
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                        ->where('education', 'PWD')
                        ->where('gender', $gender)
                        ->count();
                        echo $educ;
                    }
                @endphp
            </td>
              <td>
                @php
                    if($gender == 'all'){
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                       
                        ->count();
                        echo $educ;
                    }else{
                        $educ = DB::table('user_logs')
                        ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                        ->select('user_logs.*','users.name','users.gender','users.education')
                        ->whereDate('login_time', '>=', $start)
                        ->whereDate('login_time', '<=', $end)
                        ->whereTime('login_time', '>=', $schedule->start_period)
                        ->whereTime('login_time', '<=', $schedule->end_period)
                      
                        ->where('gender', $gender)
                        ->count();
                        echo $educ;
                    }
                @endphp
            </td>

        </tr>
    @endforeach
    <tr class="table-secondary">
        <th>Total</th>
        <td>
            @php
                if($gender == 'all'){
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'Prep')
                    ->count();
                    echo $educ;
                }else{
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'Prep')
                    ->where('gender', $gender)
                    ->count();
                    echo $educ;
                }
            @endphp
        </td>
        
        <td>
            @php
                if($gender == 'all'){
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'Elementary')
                    ->count();
                    echo $educ;
                }else{
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'Elementary')
                    ->where('gender', $gender)
                    ->count();
                    echo $educ;
                }
            @endphp
        </td>
        <td>
            @php
                if($gender == 'all'){
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'Junior High School')
                    ->count();
                    echo $educ;
                }else{
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'Junior High School')
                    ->where('gender', $gender)
                    ->count();
                    echo $educ;
                }
            @endphp
        </td>
        <td>
            @php
                if($gender == 'all'){
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'Senior High School')
                    ->count();
                    echo $educ;
                }else{
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'Senior High School')
                    ->where('gender', $gender)
                    ->count();
                    echo $educ;
                }
            @endphp
        </td>
        <td>
            @php
                if($gender == 'all'){
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'College')
                    ->count();
                    echo $educ;
                }else{
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'College')
                    ->where('gender', $gender)
                    ->count();
                    echo $educ;
                }
            @endphp
        </td>
        <td>
            @php
                if($gender == 'all'){
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'Professional')
                    ->count();
                    echo $educ;
                }else{
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'Professional')
                    ->where('gender', $gender)
                    ->count();
                    echo $educ;
                }
            @endphp
        </td>
        <td>
            @php
                if($gender == 'all'){
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'Senior Citizen')
                    ->count();
                    echo $educ;
                }else{
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'Senior Citizen')
                    ->where('gender', $gender)
                    ->count();
                    echo $educ;
                }
            @endphp
        </td>
        <td>
            @php
                if($gender == 'all'){
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'OSY')
                    ->count();
                    echo $educ;
                }else{
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'OSY')
                    ->where('gender', $gender)
                    ->count();
                    echo $educ;
                }
            @endphp
        </td>
        <td>
            @php
                if($gender == 'all'){
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'PWD')
                    ->count();
                    echo $educ;
                }else{
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('education', 'PWD')
                    ->where('gender', $gender)
                    ->count();

                    echo $educ;
                }
            @endphp
        </td>
           <td>
            @php
                if($gender == 'all'){
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                   ->count(); 
                    echo $educ;
                }else{
                    $educ = DB::table('user_logs')
                    ->join('users', 'user_logs.user_id', '=', 'users.id')                                
                    ->select('user_logs.*','users.name','users.gender','users.education')
                    ->whereDate('login_time', '>=', $start)
                    ->whereDate('login_time', '<=', $end)
                    ->where('gender', $gender)
                     ->count(); 
                    echo $educ;
                }
            @endphp
        </td>
    
    </tr>
@endif