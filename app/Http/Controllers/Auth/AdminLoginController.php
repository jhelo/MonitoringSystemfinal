<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;


class AdminLoginController extends Controller
{
	public function __construct(){
		$this->middleware('guest:admin');
	}
    public function username()
    {
        return 'username';
    }
	public function showLoginForm(){
		return view('auth.admin-login');
	}

	public function login(Request $request){
		//Validate the form data
		$this->validate($request, [
			'username' =>'required',
			'password' => 'required|min:6'
		]);

		//Attemp to log the user in
		if(Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password], $request->remember)){
			//if successful, the redirect to their intended location
			return redirect()->intended(route('admin.dashboard'));
		}

		//if unsuccessfull, then redirect back to the login
		return redirect()->back()->withInput($request->only('username', 'remember'));
	}
	
}
