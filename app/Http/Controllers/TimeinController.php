<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Events\Login;
use App\User;

class TimeinController extends Controller
{
    
    public function index(){
        return view('timein');
    }

    public function checkUser(Request $request){
		$result =0;
		$input = $request->all();
		$user = User::where('username', $input['username'])->first();
		if (Hash::check($input['password'], $user->password))
		{
			event(new Login($user));
			$result = 1;
		}else{
			$result = 0;
		}
		return $result;
    }
}
