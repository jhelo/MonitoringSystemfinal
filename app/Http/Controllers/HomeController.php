<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\Sites;
use App\Model\Visits;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sites = Sites::all();
        return view('home')->with(compact('sites'));
    }

    public function show($id)
    {
        return Sites::find($id);
    }

    public function visit_count($id){
        try{
            DB::beginTransaction();
            $visit = new Visits;
            $visit->user_id = Auth::id();
            $visit->site_visited = $id;
            if($visit->save()){
                DB::commit();
                return $this->show($id);
            }
        }catch (\Throwable $e) {
           DB::rollback();
           throw $e;
        }

    }

    public function qrcode(){
        $user = User::find(Auth::id());
        // return $qrpassword; 
        return view('viewqr')->with(compact('user'));
    }

    public function updateProfile(Request $request){
        $input = $request->all();
        $user = User::find(Auth::id());
        $user->profilePicture = $input['picture']->getClientOriginalName();
        if($user->save()){
            $input['picture']->move('img/user_profile/',$input['picture']->getClientOriginalName());
            return 'success';
        }
    }
}
