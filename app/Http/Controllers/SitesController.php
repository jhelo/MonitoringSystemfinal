<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Sites;
use App\Model\Visits;
use App\User;

class SitesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        $sites = Sites::all();
        return view('admin.sites')->with(compact('sites'));

         $logs = DB::table('visits')
            ->join('users', 'visits.user_id', '=', 'users.id')
            ->select('visits.*','users.name')
            ->orderBy('id', 'asc')
            ->get();
            // $today = date('Y-m-d H:i:s');
        $today = date('Y-m-d');
        $today = Logs::distinct()->whereDate('site_visited', $today)->get(['user_id', 'site_visited']);
        // return $today;
        $schedules = Schedule::all();
        return view('admin.sites')->with(compact('sites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        try{
            $count = Sites::count();
            if($count <= 9){
                DB::beginTransaction();
                $site = new Sites;
                $site->site_name = $input['name'];
                $site->url = $input['url'];
                $site->save();
                DB::commit();
                return 'success';
            }else{
                return 'limit';
            }
        }catch (\Throwable $e) {
           DB::rollback();
           throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $site = Sites::find($id);
        // return $user;
        return view('admin.sitesView')->with(compact('site', 'id'));
    }

    public function showSite($id){
        $site = Sites::find($id);
        return $site;
    }

    public function view(Request $request){
        $input = $request->all();
        // return $input;
        $male = 0;
        $female = 0;
        $visits = array();
        // $site = Sites::find($input['id']);
        if($input['end'] == ''){
            $visitors = Visits::distinct()->where('site_visited', $input['id'])->get(['user_id']);
        }else if($input['start'] == $input['end']){
            $visitors = Visits::distinct()->whereDate('created_at', $input['end'])->where('site_visited', $input['id'])->get(['user_id']);
        }else{
            $visitors = Visits::distinct()->whereBetween('created_at', [$input['start'], $input['end']])->where('site_visited', $input['id'])->get(['user_id']);
        }
        // return $visitors;
        foreach($visitors as $visitor){
            if($input['educ'] == 'all'){
                $info = User::where('id', $visitor->user_id)->first();
                switch($info['gender']){
                    case 'M':
                        $male = $male + 1;
                    break;
                    case 'F':
                        $female = $female +1;
                    break;
                }
            }else{
                $info = User::where('id', $visitor->user_id)->where('education', $input['educ'])->first();
                switch($info['gender']){
                    case 'M':
                        $male = $male + 1;
                    break;
                    case 'F':
                        $female = $female +1;
                    break;
                }
            }
        }

        switch($input['gender']){
            case 'M':
                
                array_push($visits, array('Male', $male));
            break;
            case 'F':
                array_push($visits, array('Female', $female));
            break;
            default:
                array_push($visits, array('Male', $male), array('Female', $female));
        }
        return response()->json($visits);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $site = Sites::find($id);
            $site->delete();
            DB::commit();
            return 'deleted';
        }catch (\Throwable $e) {
           DB::rollback();
           throw $e;
        }
    }
}
