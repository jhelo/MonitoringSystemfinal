<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Users;
use App\User;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
 


    public function users(Request $request)
    {
        if ($request->ajax())
        {
            $output="";
            $gender="";
            $dbsurvey3=DB::table('dbsurvey3')->where('name','LIKE','%'.$request->search.'%')
                                             ->orWhere('email','LIKE','%'.$request->search.'%')->get();
           

        }
    }


 
    public function index(){


        // 0-No Schooling / OSY / Unemployed
        // 1-Kinder
        // 2-K12
        // 3-Vocational
        // 4-College
        // 5-Post Graduate
        // 6-Professional
        // 7-Senior Citizens
        // 8-PWD
        // 9-Total
        $male = array();
        $female = array();
        $total = array();


        for($counter = 0; $counter <= 9; $counter++) {
            switch ($counter) {
                case '0':
                    $maleCount = User::where('gender', 'M')->where('education', 'Prep')->count();
                    $femaleCount = User::where('gender', 'F')->where('education', 'Prep')->count();
                    $sum = $maleCount + $femaleCount;
                    array_push($male, $maleCount);
                    array_push($female, $femaleCount);
                    array_push($total, $sum);
                break;
                case '1':
                    $maleCount = User::where('gender', 'M')->where('education', 'Elementary')->count();
                    $femaleCount = User::where('gender', 'F')->where('education', 'Elementary')->count();
                    $sum = $maleCount + $femaleCount;
                    array_push($male, $maleCount);
                    array_push($female, $femaleCount);
                    array_push($total, $sum);
                break;
                case '2':
                    $maleCount = User::where('gender', 'M')->where('education', 'Junior High School')->count();
                    $femaleCount = User::where('gender', 'F')->where('education', 'Junior High School')->count();
                    $sum = $maleCount + $femaleCount;
                    array_push($male, $maleCount);
                    array_push($female, $femaleCount);
                    array_push($total, $sum);
                break;
                case '3':
                    $maleCount = User::where('gender', 'M')->where('education', 'Senior High School')->count();
                    $femaleCount = User::where('gender', 'F')->where('education', 'Senior High School')->count();
                    $sum = $maleCount + $femaleCount;
                    array_push($male, $maleCount);
                    array_push($female, $femaleCount);
                    array_push($total, $sum);
                break;
                case '4':
                    $maleCount = User::where('gender', 'M')->where('education', 'College')->count();
                    $femaleCount = User::where('gender', 'F')->where('education', 'College')->count();
                    $sum = $maleCount + $femaleCount;
                    array_push($male, $maleCount);
                    array_push($female, $femaleCount);
                    array_push($total, $sum);
                break;
                case '5':
                    $maleCount = User::where('gender', 'M')->where('education', 'Professional')->count();
                    $femaleCount = User::where('gender', 'F')->where('education', 'Professional')->count();
                    $sum = $maleCount + $femaleCount;
                    array_push($male, $maleCount);
                    array_push($female, $femaleCount);
                    array_push($total, $sum);
                break;
                case '6':
                    $maleCount = User::where('gender', 'M')->where('education', 'Senior Citizen')->count();
                    $femaleCount = User::where('gender', 'F')->where('education', 'Senior Citizen')->count();
                    $sum = $maleCount + $femaleCount;
                    array_push($male, $maleCount);
                    array_push($female, $femaleCount);
                    array_push($total, $sum);
                break;
                case '7':
                    $maleCount = User::where('gender', 'M')->where('education', 'OSY')->count();
                    $femaleCount = User::where('gender', 'F')->where('education', 'OSY')->count();
                    $sum = $maleCount + $femaleCount;
                    array_push($male, $maleCount);
                    array_push($female, $femaleCount);
                    array_push($total, $sum);
                break;
                case '8':
                    $maleCount = User::where('gender', 'M')->where('education', 'PWD')->count();
                    $femaleCount = User::where('gender', 'F')->where('education', 'PWD')->count();
                    $sum = $maleCount + $femaleCount;
                    array_push($male, $maleCount);
                    array_push($female, $femaleCount);
                    array_push($total, $sum);
                break;
                
                case '9':
                    $maleCount = User::where('gender', 'M')->where('gender', 'M')->count();
                    $femaleCount = User::where('gender', 'F')->where('gender', 'F')->count();
                    $sum = $maleCount + $femaleCount;
                    array_push($male, $maleCount);
                    array_push($female, $femaleCount);
                    array_push($total, $sum);
                break;

                default:
                    # code...
                    break;
            }
        }

        // return $male;
        $stats = array();

        array_push($stats, array($male, $female, $total));

        // return $stats[0][0];
        $users = User::paginate(20);
        return view('admin.users')->with(compact('users','male','female', 'total'));
    }

    
    public function getUsersLogs(Request $request){
        $input = $request->all();
        $maleCount = User::whereDate('created_at', $input['end'])->where('gender', 'M')->where('education', 'Prep')->count();
        return $maleCount;
        $male = array();
        $female = array();
        $total = array();
        if($input['start'] == $input['end']){
            for($counter = 0; $counter <= 9; $counter++) {
                switch ($counter) {
                    case '0':
                        $maleCount = User::where('gender', 'M')->whereDate('created_at', $input['end'])->where('education', 'Prep')->count();
                        $femaleCount = User::where('gender', 'F')->whereDate('created_at', $input['end'])->where('education', 'Prep')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '1':
                        $maleCount = User::where('gender', 'M')->whereDate('created_at', $input['end'])->where('education', 'Elementary')->count();
                        $femaleCount = User::where('gender', 'F')->whereDate('created_at', $input['end'])->where('education', 'Elementary')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '2':
                        $maleCount = User::where('gender', 'M')->whereDate('created_at', $input['end'])->where('education', 'Junior High School')->count();
                        $femaleCount = User::where('gender', 'F')->whereDate('created_at', $input['end'])->where('education', 'Junior High School')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '3':
                        $maleCount = User::where('gender', 'M')->whereDate('created_at', $input['end'])->where('education', 'Senior High School')->count();
                        $femaleCount = User::where('gender', 'F')->whereDate('created_at', $input['end'])->where('education', 'Senior High School')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '4':
                        $maleCount = User::where('gender', 'M')->whereDate('created_at', $input['end'])->where('education', 'College')->count();
                        $femaleCount = User::where('gender', 'F')->whereDate('created_at', $input['end'])->where('education', 'College')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '5':
                        $maleCount = User::where('gender', 'M')->whereDate('created_at', $input['end'])->where('education', 'Professional')->count();
                        $femaleCount = User::where('gender', 'F')->whereDate('created_at', $input['end'])->where('education', 'Professional')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '6':
                        $maleCount = User::where('gender', 'M')->whereDate('created_at', $input['end'])->where('education', 'Senior Citizen')->count();
                        $femaleCount = User::where('gender', 'F')->whereDate('created_at', $input['end'])->where('education', 'Senior Citizen')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '7':
                        $maleCount = User::where('gender', 'M')->whereDate('created_at', $input['end'])->where('education', 'OSY')->count();
                        $femaleCount = User::where('gender', 'F')->whereDate('created_at', $input['end'])->where('education', 'OSY')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '8':
                        $maleCount = User::where('gender', 'M')->whereDate('created_at', $input['end'])->where('education', 'PWD')->count();
                        $femaleCount = User::where('gender', 'F')->whereDate('created_at', $input['end'])->where('education', 'PWD')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                        case '9':
                        $maleCount = User::where('gender', 'M')->whereDate('created_at', $input['end'])->where('gender', 'M')->count();
                        $femaleCount = User::where('gender', 'F')->whereDate('created_at', $input['end'])->where('gender', 'F')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    default:
                        # code...
                        break;
                }
            }
        }else{
            for($counter = 0; $counter <= 9; $counter++) {
                switch ($counter) {
                    case '0':
                        $maleCount = User::where('gender', 'M')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'Prep')->count();
                        $femaleCount = User::where('gender', 'F')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'Prep')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '1':
                        $maleCount = User::where('gender', 'M')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'Elementary')->count();
                        $femaleCount = User::where('gender', 'F')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'Elementary')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '2':
                        $maleCount = User::where('gender', 'M')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'Junior High School')->count();
                        $femaleCount = User::where('gender', 'F')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'Junior High School')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '3':
                        $maleCount = User::where('gender', 'M')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'Senior High School')->count();
                        $femaleCount = User::where('gender', 'F')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'Senior High School')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '4':
                        $maleCount = User::where('gender', 'M')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'College')->count();
                        $femaleCount = User::where('gender', 'F')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'College')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '5':
                        $maleCount = User::where('gender', 'M')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'Professional')->count();
                        $femaleCount = User::where('gender', 'F')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'Professional')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '6':
                        $maleCount = User::where('gender', 'M')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'Senior Citizen')->count();
                        $femaleCount = User::where('gender', 'F')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'Senior Citizen')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '7':
                        $maleCount = User::where('gender', 'M')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'OSY')->count();
                        $femaleCount = User::where('gender', 'F')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'OSY')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    case '8':
                        $maleCount = User::where('gender', 'M')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'PWD')->count();
                        $femaleCount = User::where('gender', 'F')->whereBetween('created_at', [$input['start'], $input['end']])->where('education', 'PWD')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                     case '9':
                        $maleCount = User::where('gender', 'M')->whereBetween('created_at', [$input['start'], $input['end']])->where('gender', 'M')->count();
                        $femaleCount = User::where('gender', 'F')->whereBetween('created_at', [$input['start'], $input['end']])->where('gender', 'F')->count();
                        $sum = $maleCount + $femaleCount;
                        array_push($male, $maleCount);
                        array_push($female, $femaleCount);
                        array_push($total, $sum);
                    break;
                    default:
                        # code...
                        break;
                }
            }
        }

        $stats = array();

        array_push($stats, array($male, $female, $total));
        
        return $stats;


     

   
    }
    

   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $user = User::find($id);
            $user->delete();
            DB::commit();
            return 'deleted';
        }catch (\Throwable $e) {
           DB::rollback();
           throw $e;
        }
    }
 


       public function updateProfile(Request $request){
        $input = $request->all();
        $user = User::find(Controller::id());
        $user->profilePicture = $input['picture']->getClientOriginalName();
        if($user->save()){
            $input['picture']->move('img/user_profile/',$input['picture']->getClientOriginalName());
            return 'success';
        }
    }
        

 public function search() {

    
    }

}


