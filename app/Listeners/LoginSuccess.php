<?php

namespace App\Listeners;

use App\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use App\Model\Terminal;
use App\Model\Logs;
use Carbon\Carbon;

class LoginSuccess
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $ip = \Request::ip();
        DB::beginTransaction();
        $terminal = Terminal::where('ip_address', $ip)->first();
        \Log::info($terminal['ip_address']);
        $logs = new Logs();
        $logs->user_id = $event->user->id;
        $logs->login_time = Carbon::now();
        $logs->terminal = $terminal['terminal_number'];
        if($logs->save()){
            DB::commit();
        }else{
            DB::rollback();
        }
    }
}
